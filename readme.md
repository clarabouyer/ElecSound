                    
                    ###################################
                    #####                         #####
                    #####   Membre du groupe:     #####
                    #####                         #####
                    #####    BOUYER & PICARD      #####
                    #####                         #####
                    ###################################

____________________________________________________________________________________

      Lien gitlab vers notre projet:
____________________________________________________________________________________

https://gitlab.com/Crashleur/ElecSound.git

Ou

https://gitlab.com/clarabouyer/ElecSound.git



____________________________________________________________________________________

      Comment créer la base de données:
____________________________________________________________________________________

Pour la creation de la bd:
./manage.py loaddb projet/al.yml



____________________________________________________________________________________

       Les commandes disponibles:
____________________________________________________________________________________

    syncdb              Met les tables a jour
    passwd              Modifie le mot de passe d'un utilisateur
    newuser             Permet de creer un nouvel utilisateur
    shell               Runs a Python shell inside Flask application context.
    runserver           Runs the Flask development server i.e. app.run()
    loaddb              Cré les tables



____________________________________________________________________________________

      Comment lancer le projet:
____________________________________________________________________________________

Etre dans le dossier ElecSound/ et lancer le server:
./manage.py runserver

------------------------------------------------------

Si ./manage.py n'est pas proposer: 
chmod +x manage.py

------------------------------------------------------

Ouvrir une page internet et écrire:
http://localhost:5000/




____________________________________________________________________________________

      Commande pour pouvoir se loguer
____________________________________________________________________________________

- Rétrograder la version de Flask-Login:
pip uninstall Flask-Login

pip instal Flask-Login==0.2.11



____________________________________________________________________________________

       Les fonctionnalités principales implémentées:
____________________________________________________________________________________

-Création d'une nouvelle base de donnée

-Création d'un yaml

-Affichage des images de chaque albums

-Page de connection

-Connexion d'un utilisateur

-Déconnexion d'un utilisateur

-Créer un compte par ligne de commande ( ./manage.py newuser login mdp )

-Consultation d'un album

-Consulter les albums fait par un artiste

-Liste des artistes

-Liste des albums d'un artiste

-Liste de tout les albums de la base

-Modification du titre de l'album en écrivant dans la barre de recherche:
http://localhost:5000/edit/music/id
Cela modifie la base de donnée. Le titre est bien modifier dans la base de donnée



____________________________________________________________________________________

       Les problèmes rencontrés:
____________________________________________________________________________________


Création de la base de données

------------------------------------------------------

Récupération des images

------------------------------------------------------

Disposition des images en ligne

------------------------------------------------------