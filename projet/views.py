from .app import app
from flask import render_template, request, redirect, url_for
from .models import *
from wtforms import PasswordField, StringField, HiddenField
from wtforms.validators import *
from hashlib import sha256
from flask.ext.login import login_user, current_user, logout_user
from flask.ext.wtf import Form 


class LoginForm(Form):
	username = StringField('Username')
	password = PasswordField('Password')
	next=HiddenField()

	def get_authenticated_user(self):
		user = User.query.get(self.username.data)
		print(user)
		if user is None:
			return None
		m = sha256()
		m.update(self.password.data.encode())
		passwd = m.hexdigest()
		return user if passwd == user.password else None



class MusicForm(Form):
	id   = HiddenField('id')
	title = StringField('title', 
		validators=[DataRequired()])
	##img = StringField('img')
	releaseYear = HiddenField('realeaseYear')
	artist_id = HiddenField('artist_id')

@app.route("/home")
@app.route("/")
def home():
	return render_template(
		"home.html",
		title="ElecSound",
		musics=get_sample())

@app.route("/albums")
def albums():
	return render_template(
		"albums.html",
		title="Albums",
		musics=get_allAlbum())

@app.route("/artistes")
def artistes():
	a=get_allArtist()
	return render_template(
		"artistes.html",
		title="Artistes",
		artistes=a)

@app.route("/edit/music/")
@app.route("/edit/music/<int:id>")
def edit_musique(id):
	"""
	if id is not None:
		return None"""
	"""
		t = get_Album(id).title
		i = get_Album(id).img
		r = get_Album(id).releaseYear
		a = get_Album(id).artist_id
	"""
	"""else:"""
	a = get_Album(id)
	f = MusicForm(id=a.id,title=a.title,releaseYear=a.releaseYear, artist_id=a.artist_id)
	return render_template("edit_music.html", music=a, form=f)
		
		
@app.route("/save/music/", methods=("POST",))
def save_music():
	a = None
	f = MusicForm()
	if f.validate_on_submit():
		id = int(f.id.data)
		a = get_Album(id)
		a.title = f.title.data
		#a.img = f.img.data
		a.releaseYear = f.releaseYear.data
		a.artist_id = f.artist_id.data
		db.session.commit()
		return redirect(url_for('unAlbum', id=id))
	a = get_Album(id)
	return render_template(
		"edit_music.html",
		music=a, form =f)

@app.route("/unAlbum/<int:id>")
def unAlbum(id):
	a=get_Album(id)
	return render_template(
		"unAlbum.html",
		title="Album",
		album=a)


@app.route("/infoArtistes/<int:id>")
def infoArtistes(id):
	a=get_AlbumByArtist(id)
	b=get_artist(id).name
	return render_template(
		"infoArtistes.html",
		title="Artiste",
		infoArtistes=a,
		nomArtiste=b)
		
@app.route("/login/", methods=("GET","POST",))
def login():
	f = LoginForm()
	if not f.is_submitted():
		f.next.data =request.args.get('')
	elif f.validate_on_submit(): 
		user = f.get_authenticated_user()
		if user:
			login_user(user)
			next = f.next.data
			return redirect(next) or url_for('home')
	return render_template(
	"login.html",form=f)


@app.route("/logout/")
def logout():
	logout_user()
	return redirect(url_for('home'))	

