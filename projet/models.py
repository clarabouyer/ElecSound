from .app import db, login_manager
from flask.ext.login import UserMixin
import yaml, os.path

"""
Music = yaml.load(
	open(
		os.path.join(
			os.path.dirname(__file__),
			"al.yml")))
"""	
class Artist(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(100))
	
class Genre(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	type = db.Column(db.String(100))
	
class Music(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	title = db.Column(db.String(120))
	img = db.Column(db.String(250))
	releaseYear = db.Column(db.Integer)
	
	artist_id = db.Column(db.Integer, db.ForeignKey("artist.id"))
	artist = db.relationship("Artist", backref=db.backref("musics", lazy="dynamic"))
	def __repr__(self):
		return "<Music (%d) %s>" % (self.id, self.title)
		
class Association(db.Model):
	id=db.Column(db.Integer, primary_key=True)
	music_id=db.Column(db.Integer,db.ForeignKey('music.id'))
	genre_id=db.Column(db.Integer,db.ForeignKey('genre.id'))
	music=db.relationship("Music", backref=db.backref("genre_asso", lazy="dynamic"))
	genre=db.relationship("Genre", backref=db.backref("music_asso", lazy="dynamic"))
	
class User(db.Model, UserMixin):
	username = db.Column(db.String(50), primary_key=True)
	password = db.Column(db.String(64))
	
	def get_id(self):
		return self.username

"""
def get_sample():
	return Music[0:10]
"""

def get_sample():
	return Music.query.limit(10).all()

def get_artist(id):
	return Artist.query.get(id)

def get_Album(id):
	return Music.query.get(id)

def get_allArtist():
	return Artist.query.all()

def get_allAlbum():
	return Music.query.all()

def get_AlbumByArtist(artist_id):
	return Music.query.filter(Music.artist_id==artist_id).all()

@login_manager.user_loader
def load_user(username):
	return User.query.get(username)
