#! /usr/bin/env python3
from flask import Flask
from flask.ext.script import Manager
from flask.ext.bootstrap import Bootstrap
from flask.ext.sqlalchemy import SQLAlchemy
#from flask.ext.mysqldb import MySQL
from flask.ext.login import LoginManager
import os.path

app=Flask(__name__)
app.debug=True
app.config['BOOTSTRAP_SERVE_LOCAL'] = True
app.config['SECRET_KEY'] = 'E01E6C6E-4921-202C-3CD7-7824AF188F34'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS']=True
app.config['SQLALCHEMY_ECHO']=True
Bootstrap(app)
manager = Manager(app)
login_manager = LoginManager(app)
login_manager.login_view="login"

def mkpath(p):
	return os.path.normpath(
			os.path.join(
				os.path.dirname(__file__),
				p))



app.config['SQLALCHEMY_DATABASE_URI'] = ('sqlite:///'+mkpath('../projet.db'))
db = SQLAlchemy(app)

