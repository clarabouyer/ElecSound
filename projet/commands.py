from .app import manager, db
import yaml
from .models import Artist, Music, Genre, User#, load_user
from hashlib import sha256

@manager.command
def loaddb(filename):
	
	db.create_all()
	music =yaml.load(open(filename))
	artists = {}
	genres = set()
	for m in music:
		a = m["by"]
		if a not in artists:
			o = Artist(name=a)
			db.session.add(o)
			artists[a]=o
		g = m["genre"]
		genres |= set(g)
	for g in genres:
		db.session.add_all([Genre(type=g)])
	db.session.commit()


	for m in music:
		a = artists[m["by"]]
		o = Music(title = m["title"], img  = m["img"], releaseYear = int(m["releaseYear"]), artist_id = a.id)
		db.session.add(o)
	db.session.commit()

"""
@manager.command
def modifdb(filename):
	music =yaml.load(open(filename))
	for m in music:
"""
		

@manager.command
def syncdb():
	db.create_all()


@manager.command
def newuser(username, password):
	m = sha256()
	m.update(password.encode())
	u = User(username=username, password=m.hexdigest())
	db.session.add(u)
	db.session.commit()
	
@manager.command
def passwd(username, passwd):
	m = sha256()
	m.update(password.encode())
	u=get_user(username)
	u.password=m.hexdigest()
	db.session.commit()
	
